# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 2, 2 )

######################################################################
def createPlane( meshtype, gridx, gridy, tx, ty, sx, sy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                         2,
                                                         [ float( 2.0 ),
                                                           float( 2.0 ) ] ) )

        if meshtype == 'TriangleStrip':
                mesh = MeshStripPlane( gridx,
                                       gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       texCoordAttribute,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        else:
                mesh = MeshTrianglePlane( gridx,
                                          gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          texCoordAttribute,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
                
        mesh.translate( [ tx, ty, 0 ] )
        mesh.scale( [ sx, sy ] )

        return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2 ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            c1,
                                            c2,
                                            2, 2,
                                            w, h,
                                            'OFF',
                                            'OPENGLES2_RGBA8888' )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        return TextureSetup( textureDataIndex, textureIndex )
                                               
######################################################################
class TrianglesViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangles viewport culling.

        Expected output: in the center location, a 2x2 checker board with
        up-left and bottom-right rectangles magenta, up-right and bottom-left
        rectangles blue. The checker board is drawn into center of the gray
        background. Different locations displace the checker board so that only
        one rectangle is visible. UpLeft=magenta, UpRight=blue,
        BottomRight=magenta, BottomLeft=blue. Viewport size 320*240.
        '''

        testValues = { ( 'location', 'meshtype' ) : [ ( 'Center',      'Triangles' ),
                                                      ( 'Center',      'TriangleStrip' ),
                                                      ( 'UpLeft',      'Triangles' ),
                                                      ( 'UpLeft',      'TriangleStrip' ),
                                                      ( 'UpRight',     'Triangles' ),
                                                      ( 'UpRight',     'TriangleStrip' ),
                                                      ( 'BottomRight', 'Triangles' ),
                                                      ( 'BottomRight', 'TriangleStrip' ),
                                                      ( 'BottomLeft',  'Triangles' ),
                                                      ( 'BottomLeft',  'TriangleStrip' ),
                                          ]
                       }
        
        def __init__( self, location, meshtype ):
                vt.VisualTestCase.__init__( self )
                self.location    = location
                self.meshtype    = meshtype
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.name = "OPENGLES2 triangles viewport culling, location=%s, mesh type=%s" % ( self.location,
                                                                                                  self.meshtype, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;        

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.location == 'Center':
                        tx = -0.5
                        ty = -0.5
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'UpLeft':
                        tx = -1.0
                        ty = 0.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'UpRight':
                        tx = 0.0
                        ty = 0.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'BottomRight':
                        tx = 0.0
                        ty = -1.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'BottomLeft':
                        tx = -1.0
                        ty = -1.0
                        sx = 4.0
                        sy = 4.0
                else:
                        raise 'Unexpected location'
                        
                mesh              = createPlane( self.meshtype, self.gridx, self.gridy, tx, ty, sx, sy )
                mesh.gradientColors( [ 1.0, 0.0, 1.0, 1.0 ], [ 1.0, 0.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 0.0, 1.0, 1.0 ] )

                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class TrianglesNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangles near and far plane culling.

        Expected output: in the center of a gray background, a 4x4 checker board
        with up-left and bottom-right rectangles magenta. The checker board
        should be missing an equally sized triangle from the bottom-left and
        top-right corner. Also, the border tiles should be partially
        clipped. Viewport size 320*240.
        '''

        testValues = { ( 'meshtype' ) : [ ( 'Triangles' ),
                                          ( 'TriangleStrip' ),
                                          ]
                       }
        
        def __init__( self, meshtype ):
                vt.VisualTestCase.__init__( self )
                self.meshtype    = meshtype
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.name = "OPENGLES2 triangles near and far plane culling mesh type=%s" % ( self.meshtype, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.meshtype, self.gridx, self.gridy, -0.5, -0.5, 2.5, 2.5 )
                mesh.gradientColors( [ 1.0, 0.0, 1.0, 1.0 ], [ 1.0, 0.0, 1.0, 1.0 ] )
                mesh.depthGradient( -2.0, 3.3333 )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 0.0, 1.0, 1.0 ] )

                useTexture( modules, textureSetup, 0, program )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
def createPlane2( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                
######################################################################
class PointsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test points viewport culling.

        Expected output: 8x8 grid of points drawn using vertex color. Vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Points are drawn
        into center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = 10
                self.gridx     = 8
                self.gridy     = 8
                self.overdraw  = 1
                self.name = "OPENGLES2 points viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                        gl_PointSize = %f;
                    }
                    """ % ( self.pointSize, )

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )
                
                mesh.indices = []
                for i in range( self.gridx * self.gridy ):
                        mesh.indices.append( i )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );

                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )                
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_POINTS',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class PointsNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test points near and far plane culling.

        Expected output: a grid of 3x3 points on a gray background, with the
        lower-left and upper-right points missing. All points should be of equal
        size. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = 10
                self.gridx     = 3
                self.gridy     = 3
                self.overdraw  = 1
                self.name = "OPENGLES2 points near and far plane culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                        gl_PointSize = %f;
                    }
                    """ % ( self.pointSize, )

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.depthGradient( -1.2, 1.5 )
                
                mesh.indices = []
                for i in range( self.gridx * self.gridy ):
                        mesh.indices.append( i )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );

                OpenGLES2.ClearColor( [ 0.3, 0.3, 0.3, 1.0 ] )                
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_POINTS',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class LinesViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test lines viewport culling.

        Expected output: a closed grid of 8 horizontal lines and 8 vertical
        lines. Vertex colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Lines are drawn
        into center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 8
                self.gridy     = 8
                self.overdraw  = 1
                self.name = "OPENGLES2 lines viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )
                
                mesh.indices = []
                for i in range( self.gridy ):
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + ( self.gridx - 1 ) )

                for i in range( self.gridx ):
                        mesh.indices.append( i )
                        mesh.indices.append( ( self.gridx - 1 ) * self.gridy + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );               
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )                                
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_LINES',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class LinesNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test lines near and far culling.

        Expected output: a closed grid of 3 horizontal lines and 3 vertical
        lines on a gray background. The grid should be culled from lower-left
        and upprt-right corner. All lines should be of equal width. Viewport
        size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 8
                self.gridx     = 3
                self.gridy     = 3
                self.overdraw  = 1
                self.name = "OPENGLES2 lines near and far culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.depthGradient( -1.2, 1.5 )
                
                mesh.indices = []
                for i in range( self.gridy ):
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + ( self.gridx - 1 ) )

                for i in range( self.gridx ):
                        mesh.indices.append( i )
                        mesh.indices.append( ( self.gridx - 1 ) * self.gridy + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );               
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )                                
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_LINES',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class LineStripsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line strips viewport culling.

        Expected output: a continuous line strip from lower-left corner to the
        lower-right corner. Vertex colors are defined so that the lower-left
        vertex has color [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. The line strip is
        drawn into center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 line strips viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )

                indicesPerStrip = 8
                mesh.indices = []
                for i in range( 1, self.gridy ):
                        if i % 2 == 1:
                                mesh.indices.append( i - 1 )                                
                                mesh.indices.append( i )
                                mesh.indices.append( i )                  # Add degerated segment
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i * self.gridx )
                        else:
                                mesh.indices.append( i * self.gridx )
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerStrip ):
                        OpenGLES2.DrawElements( 'GL_LINE_STRIP',
                                                indicesPerStrip,
                                                i * indicesPerStrip,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class LineStripsNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line strips near and far culling.

        Expected output: a line strip starting from lower-left corner and going
        to lower-right corner, on a gray background. The strip is missing a part
        from the lower-left and upper right corner. All lines should be of equal
        width. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 8
                self.gridx     = 3
                self.gridy     = 3
                self.overdraw  = 1
                self.name = "OPENGLES2 line strips near and far culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.depthGradient( -1.2, 1.5 )

                indicesPerStrip = 8
                mesh.indices = []
                for i in range( 1, self.gridy ):
                        if i % 2 == 1:
                                mesh.indices.append( i - 1 )                                
                                mesh.indices.append( i )
                                mesh.indices.append( i )                  # Add degerated segment
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i * self.gridx )
                        else:
                                mesh.indices.append( i * self.gridx )
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerStrip ):
                        OpenGLES2.DrawElements( 'GL_LINE_STRIP',
                                                indicesPerStrip,
                                                i * indicesPerStrip,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class LineLoopsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line loops viewport culling.

        Expected output: 8 brackets pointing to top-right corner, forming a
        closed line loop shape. Vertex colors are defined so that the lower-left
        vertex has color [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. The loop is drawn
        into the center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 line loops viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )

                indicesPerLoop = 7
                mesh.indices = []
                for i in range( 1, self.gridy - 1 ):
                        mesh.indices.append( i )
                        mesh.indices.append( i + 1 )
                        mesh.indices.append( i + 1 )   # Denerated segment                        
                        mesh.indices.append( ( i + 1 ) * self.gridx + i + 1 )
                        mesh.indices.append( ( i + 1 ) * self.gridx )
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerLoop ):
                        OpenGLES2.DrawElements( 'GL_LINE_LOOP',
                                                indicesPerLoop,
                                                i * indicesPerLoop,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class LineLoopsNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line loops near and far culling.

        Expected output: 8 brackets pointing to top-right corner, forming a
        closed line loop with the exception of missing line segments from the
        lower-left and the upper-right corner. All lines should be of equal
        width. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 4
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 line loops near and far culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.depthGradient( -1.1, 1.2 )

                indicesPerLoop = 7
                mesh.indices = []
                for i in range( 1, self.gridy - 1 ):
                        mesh.indices.append( i )
                        mesh.indices.append( i + 1 )
                        mesh.indices.append( i + 1 )   # Denerated segment                        
                        mesh.indices.append( ( i + 1 ) * self.gridx + i + 1 )
                        mesh.indices.append( ( i + 1 ) * self.gridx )
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerLoop ):
                        OpenGLES2.DrawElements( 'GL_LINE_LOOP',
                                                indicesPerLoop,
                                                i * indicesPerLoop,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TriangleFansViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangle fans viewport culling.

        Expected output: A rectangle on gray background. The bottom-left corner
        of the rectangle should be blueish, the top-right corner
        grayish. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 triangle fans viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                
                indicesPerFan = 11
                mesh.indices = []
                for i in range( 0, self.gridx - 2, 2 ):
                        for j in range( 0, self.gridy - 2, 2 ):
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )   # Degenerated triangle
                                mesh.indices.append( j + ( i + 0 ) * self.gridx )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerFan ):
                        OpenGLES2.DrawElements( 'GL_TRIANGLE_FAN',
                                                indicesPerFan,
                                                i * indicesPerFan,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class TriangleFansNearFarCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangle fan near and far culling.

        Expected output: A slanted rectangle on gray background. The bottom-left
        corner of the rectangle should be blueish, the top-right corner
        grayish. Left side should be higher than the right. Viewport size
        320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 triangle fans near and far culling"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.2, 1.2 ] )
                mesh.depthGradient( -2.0, 2.0 )
                
                indicesPerFan = 11
                mesh.indices = []
                for i in range( 0, self.gridx - 2, 2 ):
                        for j in range( 0, self.gridy - 2, 2 ):
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )   # Degenerated triangle
                                mesh.indices.append( j + ( i + 0 ) * self.gridx )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES2.Viewport( x, y, w, h );
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerFan ):
                        OpenGLES2.DrawElements( 'GL_TRIANGLE_FAN',
                                                indicesPerFan,
                                                i * indicesPerFan,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
