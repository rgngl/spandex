# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from lib.common import *
import math

######################################################################
def stencilFuncToStr( f ):
    return f[ 3 : ]

######################################################################
def depthFuncToStr( f ):
    return f[ 3 : ]

######################################################################
def blendEqToStr( b ):
    return b[ 8: ]

######################################################################
def blendToStr( b ):
    return b[ 3: ]

######################################################################
def paletteTypeToStr( t ):
    return t[ 3 : -4 ]
    
######################################################################
def typeToStr( t ):
    return t[ 3 : ]

######################################################################
def filterToStr( f ):
    return f[ 3 : ]

######################################################################
def toPot( v, pot ):
    if pot:
        nv = 2 ** math.ceil( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )

######################################################################
def toLowerPot( v, pot ):
    if pot:
        nv = 2 ** math.floor( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )
    
######################################################################
def wrapToStr( wrap ):
    return wrap[ 3: ] 

######################################################################
def textureTypeToStr( type ):
    return type[ 10 : ]

######################################################################
def boolToStr( b ):
    if b:
        return 'Yes'
    else:
        return 'No'

######################################################################
def arrayToStr( t ):
    return str( t ).replace( ' ', '' )

######################################################################
def colorArrayToStr( ca ):
    s = "["
    for i in range( len( ca ) ):
        s += ( "%0.2f" % ca[ i ] )
        if i < ( len( ca ) - 1 ):
            s += ','

    s += "]"
    return s

######################################################################
def formatShader( shader ):
    shader = shader.replace( "\n", "" )
    while True:
        s = shader.replace( "  ", " " )
        if s == shader:
            return '"' + s.strip() + '"'
        shader = s

######################################################################
def setupObject( modules, indexTracker, object ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    vertexArrayIndex   = -1
    colorArrayIndex    = -1
    normalArrayIndex   = -1
    texCoordArrayIndex = []
    indexArrayIndex    = -1
    
    if not object.vertices:
        raise 'Vertices undefined'
    
    vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )    
    OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' );
    OpenGLES2.AppendToArray( vertexArrayIndex, object.vertices );

    if object.colors:
        colorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )    
        OpenGLES2.CreateArray( colorArrayIndex, 'GL_UNSIGNED_BYTE' );
        OpenGLES2.AppendToArray( colorArrayIndex, object.colors );

    if object.normals:
        normalArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )    
        OpenGLES2.CreateArray( normalArrayIndex, 'GL_FLOAT' );
        OpenGLES2.AppendToArray( normalArrayIndex, object.normals );

    if object.texCoords:
        for i in range( len( object.texCoords ) ):
            texCoordArrayIndex.append( indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' ) )
            OpenGLES2.CreateArray( texCoordArrayIndex[ i ], 'GL_FLOAT' );
            OpenGLES2.AppendToArray( texCoordArrayIndex[ i ], object.texCoords[ i ] );
        
    if object.indices:
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )    
        OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' );
        OpenGLES2.AppendToArray( indexArrayIndex, object.indices );

    return ( vertexArrayIndex,
             colorArrayIndex,
             normalArrayIndex,
             texCoordArrayIndex,
             indexArrayIndex,
             len( object.indices ),
             object.drawType, )

######################################################################
def createProgram2( modules, indexTracker, vertexShaderSource, fragmentShaderSource, objectSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    
    OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
    OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )
    
    vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
    OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
    OpenGLES2.CompileShader( vsIndex )

    fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
    OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
    OpenGLES2.CompileShader( fsIndex )

    programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
    OpenGLES2.CreateProgram( programIndex )

    OpenGLES2.AttachShader( programIndex, vsIndex )
    OpenGLES2.AttachShader( programIndex, fsIndex )

    vertexAttributeName   = 'gVertex'
    colorAttributeName    = 'gColor'
    normalAttributeName   = 'gNormal'
    texCoordAttributeName = 'gTexCoord%d'
    textureName           = 'gTexture%d'

    # Connect vertex attribute arrays to corresponding shader
    # variables
    nextLocationIndex       = -1
    vertexLocationIndex     = -1
    colorLocationIndex      = -1
    normalLocationIndex     = -1
    texCoordLocationIndices = []
    textureLocationIndices  = []

    locationValue           = 0

    # Vertices are always defined
    vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
    OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
    locationValue += 1

    if objectSetup[ 1 ] >= 0:
        colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

    if objectSetup[ 2 ] >= 0:
        normalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( normalLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, normalLocationIndex, normalAttributeName )
        locationValue += 1

    if objectSetup[ 3 ]:
        for i in range( len( objectSetup[ 3 ] ) ):
            if objectSetup[ 3 ][ i ] >= 0:       
                texCoordLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.SetRegister( texCoordLocationIndices[ i ], locationValue )
                OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndices[ i ], texCoordAttributeName % i )
            
    OpenGLES2.LinkProgram( programIndex )

    # Program must be linked before we can query the uniform variable
    # (texture) locations
    #if objectSetup[ 3 ]:
    #   for i in range( len( objectSetup[ 3 ] ) ):
    #       if objectSetup[ 3 ][ i ] >= 0:

    textureLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
    OpenGLES2.GetUniformLocation( textureLocationIndices[ 0 ], programIndex, textureName % 0 )

    OpenGLES2.DeleteShader( vsIndex )
    OpenGLES2.DeleteShader( fsIndex )

    return [ programIndex,
             vertexLocationIndex,
             colorLocationIndex,
             normalLocationIndex,
             texCoordLocationIndices,
             textureLocationIndices ]

######################################################################
def createCubeProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, objectSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    
    OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
    OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )
    
    vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
    OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
    OpenGLES2.CompileShader( vsIndex )

    fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
    OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
    OpenGLES2.CompileShader( fsIndex )

    programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
    OpenGLES2.CreateProgram( programIndex )

    OpenGLES2.AttachShader( programIndex, vsIndex )
    OpenGLES2.AttachShader( programIndex, fsIndex )

    vertexAttributeName   = 'gVertex'
    colorAttributeName    = 'gColor'
    normalAttributeName   = 'gNormal'
    #texCoordAttributeName = 'gTexCoord%d'
    textureName           = 'gCubeMap%d'

    # Connect vertex attribute arrays to corresponding shader
    # variables
    nextLocationIndex       = -1
    vertexLocationIndex     = -1
    colorLocationIndex      = -1
    normalLocationIndex     = -1
    texCoordLocationIndices = []
    textureLocationIndices  = []

    locationValue           = 0

    # Vertices are always defined
    vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
    OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
    locationValue += 1

    if objectSetup[ 1 ] >= 0:
        colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

    if objectSetup[ 2 ] >= 0:
        normalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( normalLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, normalLocationIndex, normalAttributeName )
        locationValue += 1

#     if objectSetup[ 3 ]:
#         for i in range( len( objectSetup[ 3 ] ) ):
#             if objectSetup[ 3 ][ i ] >= 0:       
#                 texCoordLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
#                 OpenGLES2.SetRegister( texCoordLocationIndices[ i ], locationValue )
#                 OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndices[ i ], texCoordAttributeName % i )
            
    OpenGLES2.LinkProgram( programIndex )

    # Program must be linked before we can query the uniform variable
    # (texture) locations
    #if objectSetup[ 3 ]:
    #    for i in range( len( objectSetup[ 3 ] ) ):
    #        if objectSetup[ 3 ][ i ] >= 0:

    textureLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
    OpenGLES2.GetUniformLocation( textureLocationIndices[ 0 ], programIndex, textureName % 0 )
    #        else:
    #            textureLocationIndices.append( -1 )

    OpenGLES2.DeleteShader( vsIndex )
    OpenGLES2.DeleteShader( fsIndex )

    return [ programIndex,
             vertexLocationIndex,
             colorLocationIndex,
             normalLocationIndex,
             texCoordLocationIndices,
             textureLocationIndices ]
        
######################################################################
def useObject( modules, indexTracker, objectSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program[ 0 ] )

    OpenGLES2.VertexAttribPointer( program[ 1 ],
                                   objectSetup[ 0 ],
                                   3,
                                   'OFF',
                                   0 )
    OpenGLES2.EnableVertexAttribArray( program[ 1 ] )

    if program[ 2 ] >= 0:
        OpenGLES2.VertexAttribPointer( program[ 2 ],
                                       objectSetup[ 1 ],
                                       4,
                                       'ON',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program[ 2 ] )

    if program[ 3 ] >= 0:
        OpenGLES2.VertexAttribPointer( program[ 3 ],
                                       objectSetup[ 2 ],
                                       3,
                                       'ON',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program[ 3 ] )   

    if program[ 4 ]:
        for i in range( len( program[ 4 ] ) ):
            if program[ 4 ][ i ] >= 0:
                OpenGLES2.VertexAttribPointer( program[ 4 ][ i ],
                                               objectSetup[ 3 ][ i ],
                                               2,
                                               'OFF',
                                               0 )
                OpenGLES2.EnableVertexAttribArray( program[ 4 ][ i ] )

    OpenGLES2.ValidateProgram( program[ 0 ] )

######################################################################
def drawObject( modules, objectSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.DrawElements( objectSetup[ 6 ],
                            objectSetup[ 5 ],
                            0,
                            objectSetup[ 4 ] )

######################################################################
class VertexDataSetup:
    def __init__( self,
                  vertexArrayIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode
        
######################################################################
def setupVertexData( modules, indexTracker, mesh ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if not mesh.vertices:
        raise 'Undefined vertices'

    if not mesh.indices:
        raise 'Undefined indices'

    vertexArrayIndex        = -1
    vertexArrayNormalized   = False
    colorArrayIndex         = -1
    colorArrayNormalized    = False
    normalArrayIndex        = -1
    texCoordArrayIndices    = []
    texCoordArrayNormalized = []
    indexArrayIndex         = -1

    vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    vertexArrayNormalized = mesh.vertexNormalized
    OpenGLES2.CreateArray( vertexArrayIndex,
                           mesh.vertexGLType )
    OpenGLES2.AppendToArray( vertexArrayIndex, mesh.vertices )

    if mesh.colors:
        colorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        colorArrayNormalized = mesh.colorNormalized
        OpenGLES2.CreateArray( colorArrayIndex,
                               mesh.colorGLType )
        OpenGLES2.AppendToArray( colorArrayIndex, mesh.colors )

    if mesh.normals:
        normalArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( normalArrayIndex,
                               mesh.normalGLType )
        OpenGLES2.AppendToArray( normalArrayIndex, mesh.normals )
   
    if mesh.texCoords:
        for i in range( len( mesh.texCoords ) ):
            if mesh.texCoords:
                texCoordArrayIndices.append( indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' ) )
                texCoordArrayNormalized.append( mesh.texCoordsNormalized[ i ] )
                OpenGLES2.CreateArray( texCoordArrayIndices[ i ],
                                       mesh.texCoordsGLTypes[ i ] )
                OpenGLES2.AppendToArray( texCoordArrayIndices[ i ], mesh.texCoords[ i ] )
            else:
                texCoordArrayIndices.append( -1 )
                texCoordArrayNormalized.append( False )
            
    indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    OpenGLES2.CreateArray( indexArrayIndex, mesh.indexGLType );
    OpenGLES2.AppendToArray( indexArrayIndex, mesh.indices )

    return VertexDataSetup( vertexArrayIndex,
                            mesh.vertexComponents,
                            vertexArrayNormalized,                            
                            colorArrayIndex,
                            mesh.colorComponents,
                            colorArrayNormalized,
                            normalArrayIndex,
                            texCoordArrayIndices,
                            mesh.texCoordsComponents,
                            texCoordArrayNormalized,
                            indexArrayIndex,
                            len( mesh.indices ),
                            mesh.glMode )

######################################################################
class Program:
    def __init__( self,
                  programIndex,
                  vertexLocationIndex,
                  colorLocationIndex,
                  normalLocationIndex,
                  texCoordLocationIndices,
                  textureLocationIndices ):
        self.programIndex            = programIndex
        self.vertexLocationIndex     = vertexLocationIndex
        self.colorLocationIndex      = colorLocationIndex
        self.normalLocationIndex     = normalLocationIndex
        self.texCoordLocationIndices = texCoordLocationIndices
        self.textureLocationIndices  = textureLocationIndices
        
######################################################################
def createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    
    OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
    OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )
    
    vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
    OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
    OpenGLES2.CompileShader( vsIndex )

    fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
    OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
    OpenGLES2.CompileShader( fsIndex )

    programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
    OpenGLES2.CreateProgram( programIndex )

    OpenGLES2.AttachShader( programIndex, vsIndex )
    OpenGLES2.AttachShader( programIndex, fsIndex )

    vertexAttributeName   = 'gVertex'
    colorAttributeName    = 'gColor'
    normalAttributeName   = 'gNormal'
    texCoordAttributeName = 'gTexCoord%d'
    textureName           = 'gTexture%d'

    # Connect vertex attribute arrays to corresponding shader
    # variables
    nextLocationIndex       = -1
    vertexLocationIndex     = -1
    colorLocationIndex      = -1
    normalLocationIndex     = -1
    texCoordLocationIndices = []
    textureLocationIndices  = []

    locationValue           = 0

    # Vertices are always defined
    vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
    OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
    locationValue += 1

    if vertexDataSetup.colorArrayIndex >= 0:
        colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

    if vertexDataSetup.normalArrayIndex >= 0:
        normalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( normalLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, normalLocationIndex, normalAttributeName )
        locationValue += 1

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.SetRegister( texCoordLocationIndices[ i ], locationValue )
                OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndices[ i ], texCoordAttributeName % i )
                locationValue += 1
            else:
                texCoordLocationIndices.append( -1 )
            
    OpenGLES2.LinkProgram( programIndex )

    # Program must be linked before we can query the uniform variable
    # (texture) locations
    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                textureLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.GetUniformLocation( textureLocationIndices[ i ], programIndex, textureName % i )
            else:
                textureLocationIndices.append( -1 )

    OpenGLES2.DeleteShader( vsIndex )
    OpenGLES2.DeleteShader( fsIndex )

    return Program( programIndex,
                    vertexLocationIndex,
                    colorLocationIndex,
                    normalLocationIndex,
                    texCoordLocationIndices,
                    textureLocationIndices )

######################################################################
def useVertexData( modules, indexTracker, vertexDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.VertexAttribPointer( program.vertexLocationIndex,
                                   vertexDataSetup.vertexArrayIndex,
                                   vertexDataSetup.vertexSize,
                                   boolToSpandex( vertexDataSetup.vertexArrayNormalized ),
                                   0 )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.VertexAttribPointer( program.colorLocationIndex,
                                       vertexDataSetup.colorArrayIndex,
                                       vertexDataSetup.colorSize,
                                       boolToSpandex( vertexDataSetup.colorArrayNormalized ),
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.VertexAttribPointer( program.normalLocationIndex,
                                       vertexDataSetup.normalArrayIndex,
                                       3,
                                       boolToSpandex( True ),  # Normals always normalized
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )   

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.VertexAttribPointer( program.texCoordLocationIndices[ i ],
                                               vertexDataSetup.texCoordArrayIndices[ i ],
                                               vertexDataSetup.texCoordSizes[ i ],
                                               boolToSpandex( vertexDataSetup.texCoordArrayNormalized[ i ] ),
                                               0 )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )
    
######################################################################
def drawVertexData( modules, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.DrawElements( vertexDataSetup.glMode,
                            vertexDataSetup.indexArrayLength,
                            0,
                            vertexDataSetup.indexArrayIndex )

######################################################################
class TextureSetup:
    def __init__( self,
                  textureDataIndex,
                  textureIndex ):
        self.textureDataIndex = textureDataIndex
        self.textureIndex     = textureIndex

######################################################################
def useTexture( modules, textureSetup, textureUnit, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if textureUnit < 0 or textureUnit >= len( program.textureLocationIndices ):
        raise 'Invalid texture unit'

    OpenGLES2.ActiveTexture( textureUnit )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )

    OpenGLES2.Uniformi( program.textureLocationIndices[ textureUnit ], 1, [ textureUnit ] )
    
######################################################################
class VertexBufferDataSetup:
    def __init__( self,
                  vertexBufferIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorBufferIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalBufferIndex,
                  texCoordBufferIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexBufferIndex       = vertexBufferIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorBufferIndex        = colorBufferIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalBufferIndex       = normalBufferIndex
        self.texCoordBufferIndices   = texCoordBufferIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupVertexBufferData( modules, indexTracker, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vertexBufferIndex       = -1
    vertexArrayNormalized   = False
    colorBufferIndex        = -1
    colorArrayNormalized    = False
    normalBufferIndex       = -1
    texCoordBufferIndices   = []
    texCoordArrayNormalized = []
    indexArrayIndex         = -1

    vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( vertexBufferIndex )
    OpenGLES2.BufferData( vertexBufferIndex,
                          'ON',
                          'GL_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.vertexArrayIndex,
                          0,
                          0 )
    vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    
    if vertexDataSetup.colorArrayIndex >= 0:
        colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( colorBufferIndex )
        OpenGLES2.BufferData( colorBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.colorArrayIndex,
                              0,
                              0 )
        colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        
    if vertexDataSetup.normalArrayIndex >= 0:
        normalBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( normalBufferIndex )
        OpenGLES2.BufferData( normalBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.normalArrayIndex,
                              0,
                              0 )

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordBufferIndices.append( indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' ) )
                OpenGLES2.GenBuffer( texCoordBufferIndices[ i ] )
                OpenGLES2.BufferData( texCoordBufferIndices[ i ],
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.texCoordArrayIndices[ i ],
                                      0,
                                      0 )
                texCoordArrayNormalized.append( vertexDataSetup.texCoordArrayNormalized[ i ] )
            else:
                texCoordBufferIndices.append( -1 )
                texCoordArrayNormalized.append( False )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( indexBufferIndex )
    OpenGLES2.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.indexArrayIndex,
                          0,
                          0 )

    return VertexBufferDataSetup( vertexBufferIndex,
                                  vertexDataSetup.vertexSize,
                                  vertexArrayNormalized,
                                  colorBufferIndex,
                                  vertexDataSetup.colorSize,                                  
                                  colorArrayNormalized,
                                  normalBufferIndex,
                                  texCoordBufferIndices,
                                  vertexDataSetup.texCoordSizes,
                                  texCoordArrayNormalized,
                                  indexBufferIndex,
                                  vertexDataSetup.indexArrayLength,
                                  vertexDataSetup.glMode )

######################################################################
def useVertexBufferData( modules, indexTracker, vertexBufferDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.vertexBufferIndex,
                                         'ON',
                                         program.vertexLocationIndex,
                                         vertexBufferDataSetup.vertexSize,
                                         boolToSpandex( vertexBufferDataSetup.vertexArrayNormalized ),
                                         0 )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.colorBufferIndex,
                                             'ON',
                                             program.colorLocationIndex,
                                             vertexBufferDataSetup.colorSize,
                                             boolToSpandex( vertexBufferDataSetup.colorArrayNormalized ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.normalBufferIndex,
                                             'ON',
                                             program.normalLocationIndex,
                                             3,
                                             boolToSpandex( True ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )   

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.texCoordBufferIndices[ i ],
                                                     'ON',
                                                     program.texCoordLocationIndices[ i ],
                                                     vertexBufferDataSetup.texCoordSizes[ i ],
                                                     boolToSpandex( vertexBufferDataSetup.texCoordArrayNormalized[ i ] ),
                                                     0 )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def drawVertexBufferData( modules, vertexBufferDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  vertexBufferDataSetup.glMode,
                                  vertexBufferDataSetup.indexArrayLength,
                                  0 )    
    
######################################################################
class MeshDataSetup:
    def __init__( self,
                  meshIndex,
                  vertexArrayIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.meshIndex               = meshIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshData( modules, indexTracker, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vertexArrayIndex         = -1
    vertexArrayNormalized    = False
    colorArrayIndex          = -1
    colorArrayNormalized     = False
    normalArrayIndex         = -1
    texCoordArrayIndices     = []
    texCoordArraysNormalized = []
    nextIndex                = 0
    meshIndices              = []
    sizes                    = []
    
    vertexArrayIndex = nextIndex
    nextIndex += 1
    meshIndices.append( vertexArrayIndex )
    vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    sizes += [ vertexDataSetup.vertexSize ]
    
    if vertexDataSetup.colorArrayIndex >= 0:
        colorArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( colorArrayIndex )
        colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        sizes += [ vertexDataSetup.colorSize ]
        
    if vertexDataSetup.normalArrayIndex >= 0:
        normalArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( normalArrayIndex )
        sizes += [ 3 ]
        
    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices >= 0:
                texCoordArrayIndices.append( nextIndex )
                nextIndex += 1
                meshIndices.append( texCoordArrayIndices[ i ] )
                texCoordArraysNormalized.append( vertexDataSetup.texCoordArrayNormalized[ i ] )
                sizes += [ vertexDataSetup.texCoordSizes[ i ] ]
            else:
                texCoordArrayIndices.append( -1 )
                meshIndices.append( -1 )
                texCoordArraysNormalized.append( False )
                sizes += [ 0 ]
            
    meshIndex = indexTracker.allocIndex( 'OPENGLES2_MESH_INDEX' )
    OpenGLES2.CreateMesh( meshIndex, meshIndices, sizes )

    return MeshDataSetup( meshIndex,
                          vertexArrayIndex,
                          vertexDataSetup.vertexSize,
                          vertexArrayNormalized,
                          colorArrayIndex,
                          vertexDataSetup.colorSize,
                          colorArrayNormalized,
                          normalArrayIndex,
                          texCoordArrayIndices,
                          vertexDataSetup.texCoordSizes,
                          texCoordArraysNormalized,
                          vertexDataSetup.indexArrayIndex,
                          vertexDataSetup.indexArrayLength,
                          vertexDataSetup.glMode )

######################################################################
class MeshBufferDataSetup:
    def __init__( self,
                  meshBufferIndex,
                  vertexArrayIndex,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordArrayNormalized,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.meshBufferIndex         = meshBufferIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshBufferData( modules, indexTracker, meshDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    meshBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( meshBufferIndex )
    OpenGLES2.MeshBufferData( meshBufferIndex,
                              'ON',
                              meshDataSetup.meshIndex,
                              'GL_STATIC_DRAW' )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( indexBufferIndex )
    OpenGLES2.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          meshDataSetup.indexArrayIndex,
                          0,
                          0 )

    return MeshBufferDataSetup( meshBufferIndex,
                                meshDataSetup.vertexArrayIndex,
                                meshDataSetup.vertexArrayNormalized,
                                meshDataSetup.colorArrayIndex,
                                meshDataSetup.colorArrayNormalized,
                                meshDataSetup.normalArrayIndex,
                                meshDataSetup.texCoordArrayIndices[ : ],
                                meshDataSetup.texCoordArrayNormalized[ : ],
                                indexBufferIndex,
                                meshDataSetup.indexArrayLength,
                                meshDataSetup.glMode )
######################################################################
def useMeshData( modules, indexTracker, meshDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.MeshVertexAttribPointer( program.vertexLocationIndex,
                                       meshDataSetup.meshIndex,
                                       meshDataSetup.vertexArrayIndex,
                                       boolToSpandex( meshDataSetup.vertexArrayNormalized ) )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.MeshVertexAttribPointer( program.colorLocationIndex,
                                           meshDataSetup.meshIndex,
                                           meshDataSetup.colorArrayIndex,
                                           boolToSpandex( meshDataSetup.colorArrayNormalized ) )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.MeshVertexAttribPointer( program.normalLocationIndex,
                                           meshDataSetup.meshIndex,
                                           meshDataSetup.normalArrayIndex,
                                           boolToSpandex( True ) )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )   

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.MeshVertexAttribPointer( program.texCoordLocationIndices[ i ],
                                                   meshDataSetup.meshIndex,
                                                   meshDataSetup.texCoordArrayIndices[ i ],
                                                   boolToSpandex( meshDataSetup.texCoordArrayNormalized[ i ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )   

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                             'ON',
                                             program.vertexLocationIndex,
                                             meshBufferDataSetup.vertexArrayIndex,
                                             boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                 'ON',
                                                 program.colorLocationIndex,
                                                 meshBufferDataSetup.colorArrayIndex,
                                                 boolToSpandex( meshBufferDataSetup.colorArrayNormalized ) )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                 'ON',
                                                 program.normalLocationIndex,
                                                 meshBufferDataSetup.normalArrayIndex,
                                                 boolToSpandex( True ) )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.texCoordLocationIndices[ i ],
                                                         meshBufferDataSetup.texCoordArrayIndices[ i ],
                                                         boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ i ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def drawMeshBufferData( modules, meshBufferDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  meshBufferDataSetup.glMode,
                                  meshBufferDataSetup.indexArrayLength,
                                  0 )    
    
