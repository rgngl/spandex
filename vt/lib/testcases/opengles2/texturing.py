# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class TextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test texture wrapping.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'wrapS', 'wrapT' ) : [ ( 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                ( 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_REPEAT', 'GL_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture wrap, wrapS=%s, wrapT=%s" % ( wrapToStr( self.wrapS ),
                                                                             wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture wrapping.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top).  Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'wrapS', 'wrapT' ) : [ ( 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                             ]
                       }
        
        def __init__( self, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture wrap, wrapS=%s, wrapT=%s" % ( wrapToStr( self.wrapS ),
                                                                                  wrapToStr( self.wrapT ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy , 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class NpotTextureWrapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture wrapping. Requires
        GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top).  Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'wrapS', 'wrapT' ) : [ ( 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                ( 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_REPEAT', 'GL_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                ( 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture wrap ext, wrapS=%s, wrapT=%s" % ( wrapToStr( self.wrapS ),
                                                                                      wrapToStr( self.wrapT ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )                
                
                mesh              = createPlane( self.gridx, self.gridy , 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class TextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test texture min filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture min filter, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                   wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture min filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture min filter, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                        wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture min filtering. Requires
        GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture min filter ext, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                            wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class TextureMagFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test texture mag filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'magFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, magFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.magFilter   = magFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture mag filter, filter=%s, wrap=%s" % ( filterToStr( self.magFilter ),
                                                                                   wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toLowerPot( ( WIDTH - 1 ) / 2.5, True )[ 0 ]
                textureH = toLowerPot( ( HEIGHT - 1 )/ 2.5, True )[ 0 ]

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        self.magFilter,
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMagFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture mag filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'magFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ]
                       }
        
        def __init__( self, magFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.magFilter   = magFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture mag filter, filter=%s, wrap=%s" % ( filterToStr( self.magFilter ),
                                                                                        wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toLowerPot( ( WIDTH - 1 ) / 2.5, True )[ 0 ]
                textureH = toLowerPot( ( HEIGHT - 1 )/ 2.5, True )[ 0 ]

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        self.magFilter,
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMagFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture mag filtering. Requires
        GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'magFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, magFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.magFilter   = magFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture mag filter ext, filter=%s, wrap=%s" % ( filterToStr( self.magFilter ),
                                                                                            wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toLowerPot( ( WIDTH - 1 ) / 2.5, True )[ 0 ]
                textureH = toLowerPot( ( HEIGHT - 1 )/ 2.5, True )[ 0 ]

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        self.magFilter,
                                        self.wrap,
                                        self.wrap )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class TextureGenerateMipmap( vt.VisualTestCase ):

        ''' The purpose of the test: test texture mipmap generation.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture generate mipmap, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                         wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureGenerateMipmap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture generate mipmap. Requires
        GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT texture generate mipmap, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                             wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class TextureSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test texture subimage.

        Expected output: a full screen plane with a 4x4 vertex grid. A texture
        is drawn across the plane with GL_REPEAT wrapping mode, texture
        coordinates running from 0.0 to 2.5 (left-right, bottom-top). Lower-left
        corner of the texture should be black, upper-right quarter should
        contain a subtexture with a gradient color; lower-left corner of the
        subtexture should be white and the top-right corner should be blue
        (black with luminance textures). Viewport size 320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_LUMINANCE8' ),
                                             ( 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }

        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture subimage, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureData1Index,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT' )

                textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureData2Index,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     textureW / 2, textureH / 2,
                                                     'OFF',
                                                     self.textureType )
                
                OpenGLES2.TexSubImage2D( textureData2Index,
                                         'GL_TEXTURE_2D',
                                         textureW / 2, textureH / 2 )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class CopyTexImage( vt.VisualTestCase ):

        ''' The purpose of the test: test copy texture image. GL_RGB8_OES and
        GL_RGBA8_OES buffer formats require GL_OES_rgb8_rgba8 extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A texture
        is drawn across the plane with GL_REPEAT wrapping mode, texture
        coordinates running from 0.0 to 2.5 (left-right, bottom-top). Texture is
        divided in four rectangles, red in bottom-left (alpha 0.2), green in
        bottom-right (alpha 0.4), blue in top-left (alpha 0.6), and white in
        top-right (alpha 0.8). With luminance textures, bottom-left and
        top-right rectangles are white, others black. Textures with alpha are
        blended to [0.6,0.6,0.6,0.6] background. Dithering is disabled. Viewport
        size 320*240.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'GL_RGBA4', 'OPENGLES2_ALPHA8' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_RGBA4444' ),
                                                             ( 'GL_RGB5_A1', 'OPENGLES2_RGBA5551' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_RGB565' ),
                                                             ( 'GL_RGB8_OES', 'OPENGLES2_RGB888' ),
                                                             ( 'GL_RGBA8_OES', 'OPENGLES2_RGBA8888' ),
                                                             ]
                       }

        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES2 copy teximage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 3: ],
                                                                                             textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.bufferFormat in [ 'GL_RGB8_OES', 'GL_RGBA8_OES']:
                        OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )

                OpenGLES2.Disable( 'GL_DITHER' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.bufferFormat,
                                               textureW, textureH )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 0.20 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 0.4 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 0.6 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 0.8 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                
                textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureData1Index,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT' )

                internalFormats = { 'OPENGLES2_LUMINANCE8'        : 'GL_LUMINANCE',
                                    'OPENGLES2_ALPHA8'            : 'GL_ALPHA',
                                    'OPENGLES2_LUMINANCE_ALPHA88' : 'GL_LUMINANCE_ALPHA',
                                    'OPENGLES2_RGB565'            : 'GL_RGB',
                                    'OPENGLES2_RGB888'            : 'GL_RGB',
                                    'OPENGLES2_RGBA4444'          : 'GL_RGBA',
                                    'OPENGLES2_RGBA5551'          : 'GL_RGBA',
                                    'OPENGLES2_RGBA8888'          : 'GL_RGBA',
                                    }
                
                OpenGLES2.CopyTexImage2D( 'GL_TEXTURE_2D',
                                          0,
                                          internalFormats[ self.textureType ],
                                          0, 0,
                                          textureW, textureH )
                                          
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )               
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES2.Enable( 'GL_BLEND' )
                OpenGLES2.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions() 
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class CopyTexSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test copy texture subimage. GL_RGB8_OES and
        GL_RGBA8_OES buffer formats require GL_OES_rgb8_rgba8 extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture (ranging from [0.0,0.0,1.0,0.4] lower-left to [1.0,1.0,1.0,1.0]
        top-right) is drawn across the plane with GL_REPEAT wrapping mode,
        texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). The gradient texture shows a subimage in upper-right
        quarter, divided in four rectangles, red in bottom-left (alpha 0.2),
        green in bottom-right (alpha 0.4), blue in top-left (alpha 0.6), and
        white in top-right (alpha 0.8). With luminance textures, bottom-left and
        top-right rectangles are white, others black. Textures with alpha are
        blended to [0.6,0.6,0.6,0.6] background. Dithering is disabled. Viewport
        size 320*240.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'GL_RGBA4', 'OPENGLES2_ALPHA8' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_RGBA4444' ),
                                                             ( 'GL_RGB5_A1', 'OPENGLES2_RGBA5551' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_RGB565' ),
                                                             ( 'GL_RGB8_OES', 'OPENGLES2_RGB888' ),
                                                             ( 'GL_RGBA8_OES', 'OPENGLES2_RGBA8888' ),
                                                             ]
                       }

        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES2 copy tex subimage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 3: ],
                                                                                                 textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.bufferFormat in [ 'GL_RGB8_OES', 'GL_RGBA8_OES']:
                        OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )

                OpenGLES2.Disable( 'GL_DITHER' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]               
                
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.bufferFormat,
                                               textureW, textureH )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 4, textureH / 4 )
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 0.20 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 4, 0, textureW / 4, textureH / 4 )
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 0.4 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH / 4, textureW / 4, textureH / 4 )
                OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 0.6 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 4, textureH / 4, textureW / 4, textureH / 4 )
                OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 0.8 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                               
                textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureData1Index,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 0.4 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT' )

                OpenGLES2.CopyTexSubImage2D( 'GL_TEXTURE_2D',
                                             0,
                                             textureW / 2, textureH / 2,
                                             0, 0,
                                             textureW / 2, textureH / 2 )
                                          
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )               
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES2.Enable( 'GL_BLEND' )
                OpenGLES2.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class GenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test mipmap generation.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be white, upper-right corner should be
        white. Viewport size 320*240.
        '''
        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES2_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES2_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.hint        = hint
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 generate mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                       self.hint[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT' )
                OpenGLES2.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )
                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotGenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test mipmap generation with NPOT texture
        size. Requires GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be blue, upper-right corner should be white. Viewport
        size 320*240.
        '''
        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES2_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES2_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.hint        = hint
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 generate NPOT mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                            self.hint[ 3: ], )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT' )
                OpenGLES2.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )
                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
