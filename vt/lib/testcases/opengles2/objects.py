# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )

import math

######################################################################
RAD = 360.0 / ( 2.0 * math.pi )

######################################################################
def projectionMatrix( dY = 0.0, dZ = 0.0 ):
        matProj       = [ 0.0 ] * 16
        matProj[ 0 ]  = 1.0
        matProj[ 5 ]  = 1.0
        matProj[ 10 ] = 1.0
        matProj[ 15 ] = 1.0
        return matProj

######################################################################
def rotationMatrixX( angleX ):       
        angleX        = angleX / RAD       
        matRotX       = [ 0.0 ] * 16
        matRotX[ 0 ]  = 1.0
        matRotX[ 5 ]  = +math.cos( angleX )
        matRotX[ 6 ]  = +math.sin( angleX )
        matRotX[ 9 ]  = -math.sin( angleX )
        matRotX[ 10 ] = +math.cos( angleX )
        matRotX[ 15 ] = 1.0
        return matRotX

######################################################################
def rotationMatrixY( angleY ):
        angleY        = angleY / RAD
        matRotY       = [ 0.0 ] * 16
        matRotY[ 0 ]  = +math.cos( angleY )
        matRotY[ 2 ]  = -math.sin( angleY )
        matRotY[ 5 ]   = 1.0
        matRotY[ 8 ]  = +math.sin( angleY )
        matRotY[ 10 ] = +math.cos( angleY )
        matRotY[ 15 ] = 1.0
        return matRotY

######################################################################
def rotationMatrixZ( angleZ ):
        angleZ        = angleZ / RAD
        matRotZ       = [ 0.0 ] * 16
        matRotZ[ 0 ]  = +math.cos( angleZ )
        matRotZ[ 1 ]  = +math.sin( angleZ )
        matRotZ[ 4 ]  = -math.sin( angleZ )
        matRotZ[ 5 ]  = +math.cos( angleZ )
        matRotZ[ 10 ] = 1.0
        matRotZ[ 15 ] = 1.0
        return matRotZ

######################################################################
class TexturedCubeRotated( vt.VisualTestCase ):

        '''
        The purpose of the test: test simple object drawing.

        Expected output: Slightly rotated cube with a red-blue gradient texture,
        drawn with ortho projection on a gray background. Viewport size 320*240. 
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENGLES2 textured cube rotated"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # Shader source code
                vertexShaderSource = """
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    attribute vec4 gTexCoord0;
                    uniform vec4 gTranslate;
                    uniform mat4 gRotX;    
                    uniform mat4 gRotY;
                    uniform mat4 gRotZ;                              
                    uniform mat4 gProj;  
                    varying vec4 gVSTexCoord;
                    varying vec4 gVSColor;
                                                        
                    void main()
                    {
                       vec4 pos    = gRotX * gRotY * gRotZ * gVertex + gTranslate;
                       gl_Position = gProj * pos;
                       gVSTexCoord = gTexCoord0;
                       gVSColor    = gColor;                              
                    }
                    """
                                
                fragmentShaderSource = """
                    precision highp float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord;
                                                       
                    void main()
                    {
                       vec4 tex = texture2D( gTexture0, gVSTexCoord.xy );
                       gl_FragColor = vec4( tex.r * gVSColor.r, 
                                            tex.g * gVSColor.g, 
                                            tex.b * gVSColor.b, 
                                            tex.a * gVSColor.a );
                    }
                    """

                fsSource = formatShader( fragmentShaderSource )
                vsSource = formatShader( vertexShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                objectSetup = setupObject( modules, indexTracker, Cube( 0.5 ) )
                program     = createProgram2( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )
                                                
                # Set uniform location for projection matrix
                projLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projLocationIndex, program[ 0 ], "gProj")

                matProj = projectionMatrix()
                OpenGLES2.UniformMatrix( projLocationIndex, 1,  matProj )
                
                # Set uniform locations for matrices
                translateIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( translateIndex, program[ 0 ], "gTranslate")               

                rotXIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( rotXIndex, program[ 0 ], "gRotX")               
                rotYIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( rotYIndex, program[ 0 ], "gRotY")
                rotZIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( rotZIndex, program[ 0 ], "gRotZ")

                # Do rotations
                rotX = rotationMatrixX( 25.0 )
                rotY = rotationMatrixY( 25.0 )
                rotZ = rotationMatrixZ( 25.0 )

                OpenGLES2.Uniformf( translateIndex, 1, [ -0.2, 0.2, 0.0, 0.0 ] )               
                OpenGLES2.UniformMatrix( rotXIndex, 1, rotX )
                OpenGLES2.UniformMatrix( rotYIndex, 1, rotY )
                OpenGLES2.UniformMatrix( rotZIndex, 1, rotZ )
                
                # Create texture data
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 1.0, 0.0, 0.0, 1.0 ],
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     64, 64,
                                                     'OFF',
                                                     'OPENGLES2_RGBA8888' )
                
                # Set active texture
                OpenGLES2.ActiveTexture( 0 )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )

                OpenGLES2.Uniformi( program[ 5 ][ 0 ], 1, [ 0 ] )
                
                # Enable the depth test
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )
                
                OpenGLES2.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
                
