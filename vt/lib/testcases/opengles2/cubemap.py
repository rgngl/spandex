# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )

import math

######################################################################
RAD = 360.0 / ( 2.0 * math.pi )

######################################################################
def mvMatrix( angle ):
        angle = angle /  RAD
        mv       = [ 0 ] * 16
        mv[ 0 ]  = +math.cos( angle )
        mv[ 2 ]  = +math.sin( angle )
        mv[ 5 ]  = 1.0
        mv[ 8 ]  = -math.sin( angle )
        mv[ 10 ] = +math.cos( angle )
        mv[ 14 ] = -5.0
        mv[ 15 ] = 1.0

        return mv

######################################################################
def pMatrix( aspectRatio ):
        mp       = [ 0 ] * 16
        mp[ 0 ]  = math.cos( 0.25 ) / math.sin( 0.25 )
        mp[ 5 ]  = mp[ 0 ] * aspectRatio
        mp[ 10 ] = - 10.0 / 9.0
        mp[ 11 ] = - 1.0
        mp[ 14 ] = - 10.0 / 9.0
    
        return mp

######################################################################
class CubeMap( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap texturing.

        Expected output: left side of the surface should be magenta, the right
        side should be green. There should be a small blue triangle at the
        center top of the surface, and another yellow triangle at the center
        bottom of the surface. Viewport size 320*240.
        '''
        
        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_RGBA8888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_CLAMP_TO_EDGE' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.wrap        = wrap
                self.repeats     = 1
                self.name = "OPENGLES2 cubemap, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                              wrapToStr( self.wrap ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    varying vec4 gVSTexCoord0;
                    uniform mat4 gMVP;

                    void main()
                    {
                        gl_Position = gMVP * gVertex;
                        gVSTexCoord0.xyz = gVertex.xyz;
                    }"""
        
                fragmentShaderSource = """
                    precision mediump float;
                    uniform samplerCube gTex0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        vec3 cube = vec3( textureCube( gTex0, gVSTexCoord0.xyz ) );
                        gl_FragColor = vec4( cube, 1.0 );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]
                      
                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' )
                OpenGLES2.AppendToArray( vertexArrayIndex, vertices )

                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' )
                OpenGLES2.AppendToArray( indexArrayIndex, indices )
         
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH )
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize,
                                                          textureSize,
                                                          'OFF',
                                                          self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                        OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                                'GL_LINEAR',
                                                'GL_LINEAR',
                                                self.wrap,
                                                self.wrap )
       
                vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( vertexShaderDataIndex, vsSource )
        
                vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
                OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
                OpenGLES2.CompileShader( vertexShaderIndex )
        
                fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( fragmentShaderDataIndex, fsSource )
        
                fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
                OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
                OpenGLES2.CompileShader( fragmentShaderIndex )
        
                programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
                OpenGLES2.CreateProgram( programIndex )

                OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
                OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

                OpenGLES2.LinkProgram( programIndex )
                OpenGLES2.ValidateProgram( programIndex )
                OpenGLES2.UseProgram( programIndex )

                vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetAttribLocation( vertexLocationIndex,
                                             programIndex,
                                             'gVertex' )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                               vertexArrayIndex,
                                               3,
                                               'OFF',
                                               0 )

                OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
                mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( mvpLocationIndex,
                                              programIndex,
                                              'gMVP' )

                OpenGLES2.UniformMatrix( mvpLocationIndex,
                                         1,
                                         [  -0.222787,  0.000000, -0.918314, -0.918222,
                                             0.000000,  1.000000,  0.000000,  0.000000, 
                                             -0.516500,  0.000000,  0.396106,  0.396066,
                                             1.300805, -0.283331, -1.197603, -0.997493 ] )
        
                tex0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( tex0LocationIndex,
                                              programIndex,
                                              'gTex0' )
        
                OpenGLES2.Uniformi( tex0LocationIndex,
                                    1,
                                    [ 0 ] )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_TRIANGLES',
                                        len( indices ),
                                        0,
                                        indexArrayIndex )

                OpenGLES2.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class NpotCubeMap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cubemap texturing.

        Expected output: left side of the surface should be magenta, the right
        side should be green. There should be a small blue triangle at the
        center top of the surface, and another yellow triangle at the center
        bottom of the surface. Viewport size 320*240.
        '''
        
        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_RGBA8888', 'GL_CLAMP_TO_EDGE' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.wrap        = wrap
                self.repeats     = 1
                self.name = "OPENGLES2 NPOT cubemap, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                   wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    varying vec4 gVSTexCoord0;
                    uniform mat4 gMVP;

                    void main()
                    {
                        gl_Position = gMVP * gVertex;
                        gVSTexCoord0.xyz = gVertex.xyz;
                    }"""
        
                fragmentShaderSource = """
                    precision mediump float;
                    uniform samplerCube gTex0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        vec3 cube = vec3( textureCube( gTex0, gVSTexCoord0.xyz ) );
                        gl_FragColor = vec4( cube, 1.0 );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]
                      
                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' )
                OpenGLES2.AppendToArray( vertexArrayIndex, vertices )

                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' )
                OpenGLES2.AppendToArray( indexArrayIndex, indices )
         
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH ) - 1
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize,
                                                          textureSize,
                                                          'OFF',
                                                          self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
       
                vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( vertexShaderDataIndex, vsSource )
        
                vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
                OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
                OpenGLES2.CompileShader( vertexShaderIndex )
        
                fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( fragmentShaderDataIndex, fsSource )
        
                fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
                OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
                OpenGLES2.CompileShader( fragmentShaderIndex )
        
                programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
                OpenGLES2.CreateProgram( programIndex )

                OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
                OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

                OpenGLES2.LinkProgram( programIndex )
                OpenGLES2.ValidateProgram( programIndex )
                OpenGLES2.UseProgram( programIndex )

                vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetAttribLocation( vertexLocationIndex,
                                             programIndex,
                                             'gVertex' )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                               vertexArrayIndex,
                                               3,
                                               'OFF',
                                               0 )

                OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
                mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( mvpLocationIndex,
                                              programIndex,
                                              'gMVP' )

                OpenGLES2.UniformMatrix( mvpLocationIndex,
                                         1,
                                         [  -0.222787,  0.000000, -0.918314, -0.918222,
                                             0.000000,  1.000000,  0.000000,  0.000000, 
                                             -0.516500,  0.000000,  0.396106,  0.396066,
                                             1.300805, -0.283331, -1.197603, -0.997493 ] )
        
                tex0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( tex0LocationIndex,
                                              programIndex,
                                              'gTex0' )
        
                OpenGLES2.Uniformi( tex0LocationIndex,
                                    1,
                                    [ 0 ] )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_TRIANGLES',
                                        len( indices ),
                                        0,
                                        indexArrayIndex )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class NpotCubeMapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cubemap texturing. Requires
        GL_OES_texture_npot extension.

        Expected output: left side of the surface should be magenta, the right
        side should be green. There should be a small blue triangle at the
        center top of the surface, and another yellow triangle at the center
        bottom of the surface. Viewport size 320*240.
        '''
        
        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_RGBA8888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_MIRRORED_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.wrap        = wrap
                self.repeats     = 1
                self.name = "OPENGLES2 NPOT cubemap ext, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                       wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    varying vec4 gVSTexCoord0;
                    uniform mat4 gMVP;

                    void main()
                    {
                        gl_Position = gMVP * gVertex;
                        gVSTexCoord0.xyz = gVertex.xyz;
                    }"""
        
                fragmentShaderSource = """
                    precision mediump float;
                    uniform samplerCube gTex0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        vec3 cube = vec3( textureCube( gTex0, gVSTexCoord0.xyz ) );
                        gl_FragColor = vec4( cube, 1.0 );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]
                      
                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' )
                OpenGLES2.AppendToArray( vertexArrayIndex, vertices )

                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' )
                OpenGLES2.AppendToArray( indexArrayIndex, indices )
         
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH ) - 1
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize,
                                                          textureSize,
                                                          'OFF',
                                                          self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                        OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                                'GL_LINEAR',
                                                'GL_LINEAR',
                                                self.wrap,
                                                self.wrap )
       
                vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( vertexShaderDataIndex, vsSource )
        
                vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
                OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
                OpenGLES2.CompileShader( vertexShaderIndex )
        
                fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                OpenGLES2.SetData( fragmentShaderDataIndex, fsSource )
        
                fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
                OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
                OpenGLES2.CompileShader( fragmentShaderIndex )
        
                programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
                OpenGLES2.CreateProgram( programIndex )

                OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
                OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

                OpenGLES2.LinkProgram( programIndex )
                OpenGLES2.ValidateProgram( programIndex )
                OpenGLES2.UseProgram( programIndex )

                vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetAttribLocation( vertexLocationIndex,
                                             programIndex,
                                             'gVertex' )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                               vertexArrayIndex,
                                               3,
                                               'OFF',
                                               0 )

                OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
                mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( mvpLocationIndex,
                                              programIndex,
                                              'gMVP' )

                OpenGLES2.UniformMatrix( mvpLocationIndex,
                                         1,
                                         [  -0.222787,  0.000000, -0.918314, -0.918222,
                                             0.000000,  1.000000,  0.000000,  0.000000, 
                                             -0.516500,  0.000000,  0.396106,  0.396066,
                                             1.300805, -0.283331, -1.197603, -0.997493 ] )
        
                tex0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
                OpenGLES2.GetUniformLocation( tex0LocationIndex,
                                              programIndex,
                                              'gTex0' )
        
                OpenGLES2.Uniformi( tex0LocationIndex,
                                    1,
                                    [ 0 ] )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_TRIANGLES',
                                        len( indices ),
                                        0,
                                        indexArrayIndex )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class CubeMapSphere( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap texturing with a sphere.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE8', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE8', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_CLAMP_TO_EDGE' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.wrap        = wrap
                self.name = "OPENGLES2 cubemap sphere, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                     wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )
                
                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );               
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotCubeMapSphere( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cube map texturing with a
        sphere. Requires GL_OES_texture_npot extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_CLAMP_TO_EDGE' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_CLAMP_TO_EDGE' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.wrap        = wrap
                self.name = "OPENGLES2 NPOT cubemap sphere, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                          wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )
                
                textureW = 63
                textureH = 63
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotCubeMapSphereExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cube map texturing with a
        sphere. Requires GL_OES_texture_npot extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE8', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGB565', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGB888', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA4444', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA5551', 'GL_MIRRORED_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_REPEAT' ),
                                                     ( 'OPENGLES2_RGBA8888', 'GL_MIRRORED_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, textureType, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.wrap        = wrap
                self.name = "OPENGLES2 NPOT cubemap sphere ext, texture type=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                              wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                textureW = 63
                textureH = 63
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class CubeMapMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap min filtering.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'minFilter', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ]
                       }

        def __init__( self, textureType, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.name = "OPENGLES2 cubemap min filter, texture type=%s, minFilter=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                       filterToStr( self.minFilter ),
                                                                                                       wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = min( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'ON',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );               
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class NpotCubeMapMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cubemap min filtering. 

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'minFilter', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ]
                       }

        def __init__( self, textureType, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.name = "OPENGLES2 NPOT cubemap min filter, texture type=%s, minFilter=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                            filterToStr( self.minFilter ),
                                                                                                            wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                textureW = toPot( WIDTH, True )[ 0 ] - 1 
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotCubeMapMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cubemap min filtering. Requires
        GL_OES_texture_npot extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'minFilter', 'wrap' ) : [ ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE8', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB565', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGB888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA4444', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA5551', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                                  ( 'OPENGLES2_RGBA8888', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                                  ]
                       }

        def __init__( self, textureType, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.name = "OPENGLES2 NPOT cubemap min filter ext, texture type=%s, minFilter=%s, wrap=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                                filterToStr( self.minFilter ),
                                                                                                                wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                textureW = toPot( WIDTH, True )[ 0 ] - 1 
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'ON',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class CubeMapSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap texture subimage.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each segment should contain a
        white-to-blue diagonal gradient subtexture (white-to-black with
        luminance textures), in magenta the subtexture should be at the
        upper-right quarter. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_LUMINANCE8' ),
                                             ( 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.name = "OPENGLES2 cubemap subimage, texture type=%s" % ( textureTypeToStr( self.textureType ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureData1Index,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureData1Index, faces[ i ] );

                        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             [ 0.0, 0.0, 1.0, 1.0 ],
                                                             textureSize / 2, textureSize / 2,
                                                             'OFF',
                                                             self.textureType )
                
                        OpenGLES2.TexSubImage2D( textureData2Index,
                                                 faces[ i ],
                                                 textureSize / 2, textureSize / 2 )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class CubeMapCopyTexImage( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap copy texture
        image.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each segment should be divided in
        quarters of different shades of the segment color; in magenta segment
        the lightest shade should be at the upper-right quarter. Textures with
        alpha are blended to [0.6,0.6,0.6,0.6] background. Dithering is
        disabled. Viewport size 320*240. GL_RGB8_OES and GL_RGBA8_OES buffer
        formats require GL_OES_rgb8_rgba8 extension.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'GL_RGBA4', 'OPENGLES2_ALPHA8' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_RGBA4444' ),
                                                             ( 'GL_RGB5_A1', 'OPENGLES2_RGBA5551' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_RGB565' ),
                                                             ( 'GL_RGB8_OES', 'OPENGLES2_RGB888' ),
                                                             ( 'GL_RGBA8_OES', 'OPENGLES2_RGBA8888' ),
                                                             ]
                       }
        
        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.overdraw     = 1
                self.name = "OPENGLES2 cubemap copy teximage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 3: ],
                                                                                                     textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.bufferFormat in [ 'GL_RGB8_OES', 'GL_RGBA8_OES']:
                        OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )

                OpenGLES2.Disable( 'GL_DITHER' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.bufferFormat,
                                               textureSize, textureSize )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                colors = [ [ 1.0, 0.0, 0.0, 1.0 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1.0 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1.0 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1.0 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1.0 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1.0 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        c  = [ 0.0, 0.0, 0.0, 0.0]
                        co = colors[ i ]
                        c[ 0 ] = co[ 0 ] / 4.0
                        c[ 1 ] = co[ 1 ] / 4.0
                        c[ 2 ] = co[ 2 ] / 4.0
                        c[ 3 ] = co[ 3 ] / 4.0

                        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                        OpenGLES2.Scissor( 0, 0, textureSize / 2, textureSize /2 )
                        OpenGLES2.ClearColor( c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ] / 2.2
                        c[ 1 ] = co[ 1 ] / 2.2
                        c[ 2 ] = co[ 2 ] / 2.2
                        c[ 3 ] = co[ 3 ] / 2.2
                        OpenGLES2.Scissor( textureSize / 2, 0, textureSize / 2, textureSize /2 )
                        OpenGLES2.ClearColor( c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ] / 1.33
                        c[ 1 ] = co[ 1 ] / 1.33
                        c[ 2 ] = co[ 2 ] / 1.33
                        c[ 3 ] = co[ 3 ] / 1.33
                        OpenGLES2.Scissor( 0, textureSize / 2, textureSize / 2, textureSize / 2 )
                        OpenGLES2.ClearColor(  c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ]
                        c[ 1 ] = co[ 1 ]
                        c[ 2 ] = co[ 2 ]
                        c[ 3 ] = co[ 3 ]
                        OpenGLES2.Scissor( textureSize / 2, textureSize / 2, textureSize / 2, textureSize / 2 )
                        OpenGLES2.ClearColor(  c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                
                        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateNullTextureData( textureData1Index,
                                                         textureSize, textureSize,
                                                         'OFF',
                                                         self.textureType )

                        OpenGLES2.TexImage2D( textureData1Index, faces[ i ] );

                        internalFormats = { 'OPENGLES2_LUMINANCE8'        : 'GL_LUMINANCE',
                                            'OPENGLES2_ALPHA8'            : 'GL_ALPHA',
                                            'OPENGLES2_LUMINANCE_ALPHA88' : 'GL_LUMINANCE_ALPHA',
                                            'OPENGLES2_RGB565'            : 'GL_RGB',
                                            'OPENGLES2_RGB888'            : 'GL_RGB',
                                            'OPENGLES2_RGBA4444'          : 'GL_RGBA',
                                            'OPENGLES2_RGBA5551'          : 'GL_RGBA',
                                            'OPENGLES2_RGBA8888'          : 'GL_RGBA',
                                            }
                
                        OpenGLES2.CopyTexImage2D( faces[ i ],
                                                  0,
                                                  internalFormats[ self.textureType ],
                                                  0, 0,
                                                  textureSize, textureSize )
                                          
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )               
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES2.Enable( 'GL_BLEND' )
                               
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class CubeMapCopyTexSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap copy texture
        subimage.

        Expected output: a multisegmented sphere on a gray background. Each
        segment should display a black-and-white diagonal gradient texture (from
        lower-left to top-right), with a texture subimage at one quarter. With
        RGB textures the subimage colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each subimage should be divided
        in quarters of different shades of the segment color; in magenta segment
        the lightest shade should be at the upper-right quarter. Textures with
        alpha are blended to [0.6,0.6,0.6,0.6] background. Dithering is
        disabled. Viewport size 320*240. GL_RGB8_OES and GL_RGBA8_OES buffer
        formats require GL_OES_rgb8_rgba8 extension.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'GL_RGBA4', 'OPENGLES2_ALPHA8' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                             ( 'GL_RGBA4', 'OPENGLES2_RGBA4444' ),
                                                             ( 'GL_RGB5_A1', 'OPENGLES2_RGBA5551' ),
                                                             ( 'GL_RGB565', 'OPENGLES2_RGB565' ),
                                                             ( 'GL_RGB8_OES', 'OPENGLES2_RGB888' ),
                                                             ( 'GL_RGBA8_OES', 'OPENGLES2_RGBA8888' ),
                                                             ]
                       }
        
        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.overdraw     = 1
                self.name = "OPENGLES2 cubemap copy tex subimage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 3: ],
                                                                                                         textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.bufferFormat in [ 'GL_RGB8_OES', 'GL_RGBA8_OES']:
                        OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )

                OpenGLES2.Disable( 'GL_DITHER' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.bufferFormat,
                                               textureSize, textureSize )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                colors = [ [ 1.0, 0.0, 0.0, 1.0 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1.0 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1.0 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1.0 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1.0 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1.0 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        c  = [ 0.0, 0.0, 0.0, 0.0]
                        co = colors[ i ]
                        c[ 0 ] = co[ 0 ] / 4.0
                        c[ 1 ] = co[ 1 ] / 4.0
                        c[ 2 ] = co[ 2 ] / 4.0
                        c[ 3 ] = co[ 3 ] / 4.0

                        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                        OpenGLES2.Scissor( 0, 0, textureSize / 4, textureSize / 4 )
                        OpenGLES2.ClearColor( c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ] / 2.2
                        c[ 1 ] = co[ 1 ] / 2.2
                        c[ 2 ] = co[ 2 ] / 2.2
                        c[ 3 ] = co[ 3 ] / 2.2
                        OpenGLES2.Scissor( textureSize / 4, 0, textureSize / 4, textureSize /4 )
                        OpenGLES2.ClearColor( c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ] / 1.33
                        c[ 1 ] = co[ 1 ] / 1.33
                        c[ 2 ] = co[ 2 ] / 1.33
                        c[ 3 ] = co[ 3 ] / 1.33
                        OpenGLES2.Scissor( 0, textureSize / 4, textureSize / 4, textureSize / 4 )
                        OpenGLES2.ClearColor(  c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        c[ 0 ] = co[ 0 ]
                        c[ 1 ] = co[ 1 ]
                        c[ 2 ] = co[ 2 ]
                        c[ 3 ] = co[ 3 ]
                        OpenGLES2.Scissor( textureSize / 4, textureSize / 4, textureSize / 4, textureSize / 4 )
                        OpenGLES2.ClearColor(  c[ : ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                        OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                
                        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureData1Index,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             [ 0.0, 0.0, 0.0, 0.0 ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize, textureSize,
                                                             'ON',
                                                             self.textureType )

                        OpenGLES2.TexImage2D( textureData1Index, faces[ i ] );

                        OpenGLES2.CopyTexSubImage2D( faces[ i ],
                                                     0,
                                                     textureSize / 2, textureSize / 2,
                                                     0, 0,
                                                     textureSize / 2, textureSize / 2 )
                                          
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )               
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES2.Enable( 'GL_BLEND' )
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class CubeMapGenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test cubemap texture mipmap generation.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES2_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES2_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.hint        = hint
                self.name = "OPENGLES2 cubemap generate mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                               self.hint[ 3: ], )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )
                
                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );

                OpenGLES2.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )
                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_CUBE_MAP' )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotCubeMapGenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT cubemap texture mipmap
        generation. Requires GL_OES_texture_npot extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Each color should have a slight
        gradient to white. With magenta center the white spot should be at the
        top-right corner. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES2_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES2_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES2_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES2_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self)
                self.textureType = textureType
                self.hint        = hint
                self.name = "OPENGLES2 NPOT cubemap generate mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                    self.hint[ 3: ], )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                textureW = 64 - 1
                textureH = 64 - 1
                textureSize = max( textureW, textureH )
        
                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                                             colors[ i ],
                                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                                             textureSize,
                                                             textureSize,
                                                             'OFF',
                                                             self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );

                OpenGLES2.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )
                OpenGLES2.GenerateMipmap( 'GL_TEXTURE_CUBE_MAP' )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
