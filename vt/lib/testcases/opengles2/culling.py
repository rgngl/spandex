# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

######################################################################
def createPlane( gridx, gridy, winding ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               winding,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
       
######################################################################
class PolygonCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test polygon culling.

        Expected output: a full screen plane with a 8x8 vertex triangle
        grid. Culling specifies the used cull parameter. Winding specifies the
        triangle winding. In alternating winding, first triangle column from the
        left is specified using CCW, the second using CW and third again using
        CCW. Surface is cleared to [0.0,0.0,0.5,1.0]. Viewport size 320*240.
        '''

        testValues = { ( 'culling', 'winding' ) : [ ( 'GL_NONE', 'CCW' ),
                                                    ( 'GL_NONE', 'CW' ),
                                                    ( 'GL_NONE', 'ALTERNATING' ),
                                                    ( 'GL_FRONT', 'CCW' ),
                                                    ( 'GL_FRONT', 'CW' ),
                                                    ( 'GL_FRONT', 'ALTERNATING' ),
                                                    ( 'GL_BACK', 'CCW' ),
                                                    ( 'GL_BACK', 'CW' ),
                                                    ( 'GL_BACK', 'ALTERNATING' ),
                                                    ( 'GL_FRONT_AND_BACK', 'CCW' ),
                                                    ( 'GL_FRONT_AND_BACK', 'CW' ),
                                                    ( 'GL_FRONT_AND_BACK', 'ALTERNATING' )
                                                    ]
                       }
                                                    
        def __init__( self, culling, winding ):
                vt.VisualTestCase.__init__( self )
                self.culling  = culling
                self.winding  = winding
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 polygon culling, culling=%s, winding=%s" % ( self.culling[ 3 : ],
                                                                                    self.winding, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                if self.winding == 'CCW':
                    winding = MESH_CCW_WINDING
                elif self.winding == 'CW':
                    winding = MESH_CW_WINDING
                elif self.winding == 'ALTERNATING':
                    winding = MESH_ALTERNATING_WINDING
                else:
                    raise 'Invalid winding'

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, winding )
                mesh.gradientColors( [ 0.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                if self.culling != 'GL_NONE':
                    OpenGLES2.CullFace( self.culling )
                    OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 0.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class PolygonFrontFace( vt.VisualTestCase ):

        ''' The purpose of the test: test polygon front face.

        Expected output: the test draws a full screen plane with a 8x8 vertex
        triangle grid. The first triangle column from the left is specified
        using CCW, the second using CW, third again using CCW, and so
        forth. Front face specifies how the triangle front face is
        determined. Backfacing triangles are culled. Surface is cleared to
        [0.0,0.0,0.5,1.0]. Viewport size 320*240.
        '''

        testValues = { ( 'frontFace' ) : [ ( 'GL_CCW' ),
                                           ( 'GL_CW' ),
                                           ]
                       }
                                                    
        def __init__( self, frontFace ):
                vt.VisualTestCase.__init__( self )
                self.frontFace = frontFace
                self.repeats   = 1
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 polygon front face, front face=%s" % ( self.frontFace[ 3 : ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, MESH_ALTERNATING_WINDING )
                mesh.gradientColors( [ 0.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.FrontFace( self.frontFace )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Enable( 'GL_CULL_FACE' )               
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 0.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
