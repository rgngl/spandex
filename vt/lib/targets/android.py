#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Symbio.
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# ----------------------------------------------------------------------
from lib.common import *
from lib.egl    import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return set( [ 'EGL_LIMITED' ] ).issuperset( features )

# ----------------------------------------------------------------------
def getDefaultAttributes( apis ):
    attributes = {}

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    attributes[ CONFIG_BUFFERSIZE ]     = 32
    attributes[ CONFIG_REDSIZE ]        = 8
    attributes[ CONFIG_GREENSIZE ]      = 8
    attributes[ CONFIG_BLUESIZE ]       = 8
    attributes[ CONFIG_ALPHASIZE ]      = 8
    attributes[ CONFIG_SAMPLE_BUFFERS ] = 0
    
    if api in [ API_OPENGLES1, API_OPENGLES2 ]:
        attributes[ CONFIG_DEPTHSIZE ]      = '>=16'

    if api == API_OPENVG:
        attributes[ CONFIG_ALPHAMASKSIZE ]  = '>=8'

    return attributes

# ----------------------------------------------------------------------
def setup( modules, indexTracker, width, height, apis, attributes = {} ):
    Egl = modules[ 'Egl' ]

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
    Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
    Egl.Initialize( displayIndex )

    if api == API_OPENVG:
        Egl.BindApi( 'EGL_OPENVG_API' )

    if not attributes:
        attributes = getDefaultAttributes( api )
        
    attribs = getEglAttributes( api, SURFACE_PBUFFER, attributes )
        
    configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    Egl.RGBConfig( displayIndex,
                   configIndex,
                   attribs[ CONFIG_BUFFERSIZE ],
                   attribs[ CONFIG_REDSIZE ],
                   attribs[ CONFIG_GREENSIZE ],
                   attribs[ CONFIG_BLUESIZE ],
                   attribs[ CONFIG_ALPHASIZE ],
                   attribs[ CONFIG_DEPTHSIZE ],
                   attribs[ CONFIG_ALPHAMASKSIZE ],
                   attribs[ CONFIG_SAMPLE_BUFFERS ],
                   attribs[ CONFIG_SAMPLES ],
                   attribs[ CONFIG_STENCILSIZE ],
                   attribs[ CONFIG_SUPPORTRGBTEXTURES ],
                   attribs[ CONFIG_SUPPORTRGBATEXTURES ],
                   attribs[ CONFIG_SURFACETYPE ],
                   attribs[ CONFIG_RENDERABLETYPE ],
                   )

    surfaceAttributes = []
    surfaceAttributes.append( EGL_DEFINES[ 'EGL_WIDTH' ] )
    surfaceAttributes.append( width )
    surfaceAttributes.append( EGL_DEFINES[ 'EGL_HEIGHT' ] )
    surfaceAttributes.append( height )

    surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    Egl.CreatePbufferSurfaceExt( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 surfaceAttributes )    

    apiVersion = -1
    if api == API_OPENGLES2:
        apiVersion = 2
       
    contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, apiVersion )
    Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
    
    return ( displayIndex, configIndex, surfaceIndex, contextIndex, )

# ----------------------------------------------------------------------
def outputPath( apis ):

    assert len( apis ) == 1
    api = apis[ 0 ]

    if api == API_OPENGLES1:
        return '"/sdcard/gles1out"'
    if api == API_OPENGLES2:
        return '"/sdcard/gles2out"'

    return 'Not supported'

