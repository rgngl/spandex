#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# ----------------------------------------------------------------------
from lib.common import *
from lib.egl    import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return set( [ 'EGL_LIMITED',
                  'EGL_FULL' ] ).issuperset( features )

# ----------------------------------------------------------------------
def getDefaultAttributes( apis ):
    attributes = {}

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    if api != API_OPENGLES2:
        raise 'Not supported'
    
    attributes[ CONFIG_BUFFERSIZE ]     = 32
    attributes[ CONFIG_REDSIZE ]        = 8
    attributes[ CONFIG_GREENSIZE ]      = 8
    attributes[ CONFIG_BLUESIZE ]       = 8
    attributes[ CONFIG_ALPHASIZE ]      = 8
    attributes[ CONFIG_DEPTHSIZE ]      = '>=16'
    attributes[ CONFIG_SAMPLE_BUFFERS ] = 0

    return attributes

# ----------------------------------------------------------------------
def setup( modules, indexTracker, width, height, apis, attributes = {} ):
    Egl = modules[ 'Egl' ]

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    if api != API_OPENGLES2:
        raise 'Not supported'
    
    # AMD WIN32 OPENGLES2 wrapper supports only window surfaces.
    displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
    Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
    Egl.Initialize( displayIndex )
    
    eglConfigAttributes = getEglAttributeList( api, SURFACE_WINDOW, attributes )
    
    configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    Egl.Config( displayIndex, configIndex, eglConfigAttributes )

    windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )    
    Egl.CreateWindow( windowIndex,
                      'SCT_SCREEN_DEFAULT', 
                      0,
                      0,
                      width,
                      height,
                      'SCT_COLOR_FORMAT_DEFAULT' )
    
    surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    Egl.CreateWindowSurfaceExt( displayIndex,
                                surfaceIndex,
                                configIndex,
                                windowIndex,
                                [] )    

    contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )
    Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

    return ( displayIndex, configIndex, surfaceIndex, contextIndex, )

# ----------------------------------------------------------------------
def outputPath( apis ):
    
    assert len( apis ) == 1
    api = apis[ 0 ]

    if api != API_OPENGLES2:
        raise 'Not supported'
    
    return '"gles2out"'

