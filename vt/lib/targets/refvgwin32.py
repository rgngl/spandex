#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# ----------------------------------------------------------------------
from lib.common import *
from lib.egl    import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return set( [ 'EGL_FULL',
                  'EGL_LIMITED',
                  'VGU' ] ).issuperset( features )

# ----------------------------------------------------------------------
def getDefaultAttributes( apis ):

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    if api != API_OPENVG:
        raise 'Not supported'
       
    return {}

# ----------------------------------------------------------------------
def setup( modules, indexTracker, width, height, apis, attributes = {} ):
    Egl = modules[ 'Egl' ]

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    if api != API_OPENVG:
        raise 'Not supported'
    
    displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
    Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
    Egl.Initialize( displayIndex )

    Egl.BindApi( 'EGL_OPENVG_API' )
    
    configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    Egl.RGBConfig( displayIndex,
                   configIndex,             # ConfigIndex
                   32,                      # BufferSize
                   8,                       # RedSize
                   8,                       # GreenSize
                   8,                       # BlueSize
                   8,                       # AlphaSize
                   0,                       # DepthSize
                   8,                       # AlphaMaskSize
                   -1,                      # SampleBuffers
                   -1,                      # Samples
                   0,                       # StencilSize
                   'EGL_FALSE',             # SupportRGBTextures
                   'EGL_FALSE',             # SupportRGBATextures
                   [ 'EGL_PBUFFER_BIT' ],   # SurfaceType
                   [ 'EGL_OPENVG_BIT' ] )   # RenderableType

    surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    Egl.CreatePbufferSurface( displayIndex,
                              surfaceIndex,               # SurfaceIndex
                              configIndex,                # ConfigIndex
                              width,                      # Width
                              height,                     # Height
                              'EGL_NO_TEXTURE',           # TextureFormat
                              'EGL_NO_TEXTURE',           # TextureTarget
                              'OFF',                      # MipmapTexture
                              'OFF',                      # LargestPbuffer        
                              'EGL_COLORSPACE_sRGB',      # ColorSpace
                              'EGL_ALPHA_FORMAT_NONPRE' ) # AlphaFormat

    contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )
    Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

    return ( displayIndex, configIndex, surfaceIndex, contextIndex, )

# ----------------------------------------------------------------------
def outputPath( apis ):

    assert len( apis ) == 1
    api = apis[ 0 ]
    
    if api == API_OPENVG:
        return '"./vgout"'
    
    raise 'Not supported'
