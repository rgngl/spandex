#!/bin/bash
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

TARGET=$1
SCRIPT_HOME=$PWD

# We need a target to use when generating benchmarks and visual tests. 
if [ -z $TARGET ]
    then
    echo "Undefined target."
    exit 0
fi

# Make it possible to use custom benchmark or visual test suites. For example,
# some platform still under development might not work with the full suite. The
# script uses custom suite only when available, otherwise it uses the default
# suite. Custom files are named benchmark_custom.py (benchmarks) or
# suite_custom.py (visual tests).
if [ -n "$2" ]
    then
    CUSTOM=_$2
fi

# OpenVG benchmarks
cd $SPANDEX_HOME/bd/benchmarks/openvg
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/vg11_perf_benchmark.txt
make clean

# OpenGLES1 benchmarks
cd $SPANDEX_HOME/bd/benchmarks/opengles1
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/gl11_perf_benchmark.txt
make clean

# OpenGLES2 benchmarks
cd $SPANDEX_HOME/bd/benchmarks/opengles2
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/gl20_perf_benchmark.txt
make clean

# UI benchmarks
cd $SPANDEX_HOME/bd/benchmarks/ui
make init

BENCHMARK=benchmark_openvg.py
if [ -e "benchmark_openvg$CUSTOM.py" ]
    then
    BENCHMARK=benchmark_openvg$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/vg11_ui_benchmark.txt

BENCHMARK=benchmark_opengles1.py
if [ -e "benchmark_opengles1$CUSTOM.py" ]
    then
    BENCHMARK=benchmark_opengles1$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/gl11_ui_benchmark.txt

BENCHMARK=benchmark_opengles2.py
if [ -e "benchmark_opengles2$CUSTOM.py" ]
    then
    BENCHMARK=benchmark_opengles2$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/gl20_ui_benchmark.txt
make clean

# UI2 benchmarks
cd $SPANDEX_HOME/bd/benchmarks/ui2
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/gl20_ui2_benchmark.txt
make clean

# OpenVG + OpenGLES1 benchmarks
cd $SPANDEX_HOME/bd/benchmarks/openvg_opengles1
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/vg11_gl11_perf_benchmark.txt
make clean

# OpenVG + OpenGLES2 benchmarks
cd $SPANDEX_HOME/bd/benchmarks/openvg_opengles2
make init

BENCHMARK=benchmark.py
if [ -e "benchmark$CUSTOM.py" ]
    then
    BENCHMARK=benchmark$CUSTOM.py
fi

python $BENCHMARK $TARGET > $SCRIPT_HOME/vg11_gl20_perf_benchmark.txt
make clean

# OpenVG visual test
cd $SPANDEX_HOME/vt/suites/openvg
make init

BENCHMARK=suite.py
if [ -e "suite$CUSTOM.py" ]
    then
    BENCHMARK=suite$CUSTOM.py
fi

python $BENCHMARK generate $TARGET > $SCRIPT_HOME/vg11_vt_benchmark.txt
make clean

# OpenGLES1 visual test
cd $SPANDEX_HOME/vt/suites/opengles1
make init

BENCHMARK=suite.py
if [ -e "suite$CUSTOM.py" ]
    then
    BENCHMARK=suite$CUSTOM.py
fi

python $BENCHMARK generate $TARGET > $SCRIPT_HOME/gl11_vt_benchmark.txt
make clean

# OpenGLES2 visual test
cd $SPANDEX_HOME/vt/suites/opengles2
make init

BENCHMARK=suite.py
if [ -e "suite$CUSTOM.py" ]
    then
    BENCHMARK=suite$CUSTOM.py
fi

python $BENCHMARK generate $TARGET > $SCRIPT_HOME/gl20_vt_benchmark.txt
make clean
