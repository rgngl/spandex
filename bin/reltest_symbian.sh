#!/bin/bash
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

TARGET=$1
INSTALL_PATH=$2
CUSTOM=$3
SCRIPT_HOME=$PWD

if [ -z $TARGET ]
    then
    echo "Undefined target."
    exit 0
fi

if [ -z $INSTALL_PATH ]
    then
    echo "Undefined install path."
    exit 0
fi

$SPANDEX_HOME/bin/reltest.sh $TARGET $INSTALL_PATH $CUSTOM
cp $SPANDEX_HOME/sct/symbian/common/release/autoexec.bat $INSTALL_PATH